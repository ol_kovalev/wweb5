<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  
  $messages = array();

  if (!empty($_COOKIE['save'])) {
   
    setcookie('save', '', 100000);
    setcookie('login', '', 100000); 
    setcookie('pass', '', 100000); 
    
    $messages[] = 'Спасибо, результаты сохранены.';
  }
    if (!empty($_COOKIE['pass'])) {
        $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
          и паролем <strong>%s</strong> для изменения данных.',
          strip_tags($_COOKIE['login']),
          strip_tags($_COOKIE['pass']));
  }

  
  $errors = array();
  $errors['fname'] = !empty($_COOKIE['fname_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['kol'] = !empty($_COOKIE['kol_error']);
  $errors['gender1'] = !empty($_COOKIE['gender1_error']);
  $errors['power'] = !empty($_COOKIE['power_error']);
  $errors['ok'] = !empty($_COOKIE['ok_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  
  if ($errors['fname']) {
    setcookie('fname_error', '', 100000);
    
    $messages[] = '<div class="error">Заполните имя корректно.</div>';
  }
  if ($errors['email']) {
    
    setcookie('email_error', '', 100000);
    
    $messages[] = '<div class="error">Заполните email корректно.</div>';
  }

  if ($errors['kol']) {
    
    setcookie('kol_error', '', 100000);
    
    $messages[] = '<div class="error">Укажите количество ваших конечностей.</div>';
  }

  if ($errors['gender1']) {
    
    setcookie('gender1_error', '', 100000);
    
    $messages[] = '<div class="error">Укажите свой пол.</div>';
  }

  if ($errors['power']) {
    
    setcookie('power_error', '', 100000);
    
    $messages[] = '<div class="error">Укажите свои суперспособности.</div>';
  }

  if ($errors['biography']) {
   
    setcookie('biography_error', '', 100000);
    
    $messages[] = '<div class="error">Заполните информацию о себе корректно.</div>';
  }

  if ($errors['ok']) {
    
    setcookie('ok_error', '', 100000);
    
    $messages[] = '<div class="error">Поставьте галочку.</div>';
  }

  $values = array();
  $values['fname'] = empty($_COOKIE['fname_value']) ? '' : strip_tags($_COOKIE['fname_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['gender1'] = empty($_COOKIE['gender1_value']) ? '' : $_COOKIE['gender1_value'];
  $values['kol'] = empty($_COOKIE['kol_value']) ? '' : $_COOKIE['kol_value'];
  $values['power'] = empty($_COOKIE['power_value']) ? '' : $_COOKIE['power_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);

  
  $er=false;
  foreach($errors as $el)
    if ($el == true)
      $er = true;
  if (empty($er) && !empty($_COOKIE[session_name()]) && 
  session_start() && !empty($_SESSION['login'])) {

  $user = 'u24174';
  $pass = '34623664';
  $db = new PDO('mysql:host=localhost;dbname=u24174', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $uid = $_SESSION['uid'];
  $res= $db->query("SELECT fname, email, year, gender1, kol, biography FROM application2 WHERE id = $uid");
  foreach($res as $el){
    $values['fname']=strip_tags($el['fname']);
    $values['email']=strip_tags($el['email']);
    $values['year']=strip_tags($el['year']);
    $values['gender1']=strip_tags($el['gender1']);
    $values['kol']=strip_tags($el['kol']);
    $values['biography']=strip_tags($el['biography']);
  }
  $res= $db->query("SELECT nom_spw FROM spw WHERE id = $uid");
  $sup = array();
  foreach($res as $el){
    $sup[]=(int)strip_tags($el['nom_spw']);
  }
  $sp = implode('',$sup);
  $values['power'] =$sp;
printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
}

  include('forma.php');
}

else {
  
  setlocale(LC_ALL, "ru_RU.UTF-8");
  $errors = FALSE;
  if (empty($_POST['fname']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['fname'])) {
    
    setcookie('fname_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    
    setcookie('fname_value', $_POST['fname'], time() + 30 * 24 * 60 * 60);
  }


  if (empty($_POST['email'])) {
    
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['gender1'])) {
    
    setcookie('gender1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    
    setcookie('gender1_value', $_POST['gender1'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['kol'])) {
     
    setcookie('kol_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    
    setcookie('kol_value', $_POST['kol'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['power'])) {
     
    setcookie('power_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $sp = implode('',$_POST['power']);
    
    setcookie('power_value', $sp, time() + 30 * 24 * 60 * 60);
  }


  if (empty($_POST['biography'])) {
    
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['ok'])) {
    
    setcookie('ok_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
  

  if ($errors) {
    
    header('Location: index.php');
    exit();
  }
  else {
    
    setcookie('fname_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('gender1_error', '', 100000);
    setcookie('kol_error', '', 100000);
    setcookie('power_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('ok_error', '', 100000);
    
    
  }

  $user = 'u24174';
  $pass = '34623664';
  $db = new PDO('mysql:host=localhost;dbname=u24174', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    if (!empty($_COOKIE[session_name()]) &&
  session_start() && !empty($_SESSION['login'])) {

try {
  $uid = $_SESSION['uid'];
  $stmt = $db->prepare("UPDATE application2 SET fname = ?, email = ?, year = ?, gender1 = ?, kol = ?, biography = ? WHERE id = $uid");
  $stmt -> execute([$_POST['fname'],$_POST['email'],$_POST['year'],$_POST['gender1'],$_POST['kol'],$_POST['biography']]);
  
  $db->query("DELETE FROM spw WHERE id = $uid");
  $stmt = $db->prepare("INSERT INTO spw SET id = ?, nom_spw = ?");
  foreach($_POST['power'] as $el)
    $stmt -> execute([$uid,$el]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
}
  else {

$login = substr(uniqid(time()),1,8);
$pass = substr(md5($_POST['email']),5,8);

setcookie('login', $login);
setcookie('pass', $pass);



try {
  $str = implode(',',$_POST['power']);
  
  $stmt = $db->prepare("INSERT INTO application2 SET fio = ?, email = ?, year = ?, pol = ?, limb = ?, biography = ?");
  $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['year'],$_POST['gender'],$_POST['limbs'],$_POST['biography']]);

  $id = $db->lastInsertId();
  $stmt = $db->prepare("INSERT INTO baza SET id = ?, login = ?, pass = ?");
  $stmt -> execute([$id,$login,md5($pass)]);

  $stmt = $db->prepare("INSERT INTO spw SET id = ?, nom_spw = ?");
  foreach($_POST['power'] as $el)
    $stmt -> execute([$id,$el]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
}

  
  setcookie('save', '1');

 
  header('Location: index.php');
}
